import Home from './components/Home.vue'
import Services from './components/pages/Services'
import AboutLawyer from './components/pages/AboutLawyer'
import Advantages from './components/pages/Advantages.vue'
import Certificates from './components/pages/Certificates'
import Contacts from './components/pages/Contacts'

export const routes = [
  { path: '', component: Home },
  { path: '/services', component: Services },
  { path: '/aboutLawyer', component: AboutLawyer },
  { path: '/advantages', component: Advantages },
  { path: '/certificates', component: Certificates },
  { path: '/contacts', component: Contacts }
]
